<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>

<html>
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <title>Login</title>
</head>
<body>
<div class="container">
<h1>Login:</h1>

<form action="login" method="post">
    <div class="form-group">
        <label for="email">Email address</label>
        <input type="text" class="form-control" id="email" name="email" placeholder="Enter email" required>
    </div>
    <div class="form-group">
        <label for="senha">Password</label>
        <input type="password" class="form-control" id="senha" name="senha" placeholder="Password" required>
    </div>
    <br>
    <button type="submit" class="btn btn-primary" value="login" name="login">Login</button>
</form>

    <h3><a href="cad">Criar Conta</a></h3>

<c:if test="${not empty erro}">
    <h2>
        <b>${erro}</b>
    </h2>
</c:if>
</div>
</body>
</html>
