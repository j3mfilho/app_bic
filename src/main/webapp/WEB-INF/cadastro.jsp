<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>

<html>
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <title>Cadastrar</title>
</head>
<body>
<div class="container">
    <h1>Cadastro:</h1>


    <form action="cad" method="post">
        <div class="form-group">
            <label for="nome">Your name</label>
            <input type="text" class="form-control" id="nome" name="nome" placeholder="Your name" >
        </div>
        <div class="form-group">
            <label for="email">Email address</label>
            <input type="text" class="form-control" id="email" name="email" placeholder="Enter email" >
        </div>
        <div class="form-group">
            <label for="senha">Password</label>
            <input type="password" class="form-control" id="senha" name="senha" placeholder="Password" >
        </div>
        <br><button type="submit" class="btn btn-primary" value="cad" name="cad">Cadastro</button>
        <br><br><h3><a href="login">Login</a></h3>
    </form>
    <c:if test="${not empty erro}">
        <h2>
            <b>${erro}</b>
        </h2>
    </c:if>
</div>
</body>
</html>
