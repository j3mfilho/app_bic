package br.csi.dao;

import br.csi.model.Usuario;
import br.csi.model.Permissao;

import java.sql.*;
import java.util.Calendar;
import java.util.Locale;

public class UsuarioDao {

    private String sql;
    private PreparedStatement preparedStatement;
    private ResultSet resultSet;


    public Usuario getUsuario(String email){
        Usuario usuario = null;


        try (Connection connection = new ConectaDBPostgres().getConexao()){
            this.sql = " SELECT * FROM usuario,permissao WHERE email = ? and usuario.id_permissao = permissao.id_permissao" ;
            this.preparedStatement = connection.prepareStatement(this.sql);
            this.preparedStatement.setString(1, email);
            this.resultSet = this.preparedStatement.executeQuery();
            System.out.println(this.sql);
            while (resultSet.next()){
                usuario = new Usuario();
                usuario.setId(resultSet.getInt("id_usuario"));
                usuario.setNome(resultSet.getString("nome"));
                usuario.setEmail(resultSet.getString("email"));
                usuario.setSenha(resultSet.getString("senha"));
                /*Coloca a variavel do tipo Permissao em uma variavel local para poder estanciar ela ao LoginController*/
                Permissao p = new Permissao(resultSet.getInt("id_permissao"), resultSet.getString("nome_permissao"));
                usuario.setPermissao(p);

            }


        }catch (SQLException e){
            e.printStackTrace();
        }
        return usuario;
    }
    public Usuario setUsuario(Usuario u){

        Calendar calendar = Calendar.getInstance();
        java.sql.Date startDate = new java.sql.Date(calendar.getTime().getTime());

        try (Connection connection = new ConectaDBPostgres().getConexao()) {

            this.sql = "INSERT INTO usuario (nome, email, senha, data_cadastro, ativo, id_permissao) VALUES (?, ?, ?, ?, ?, ?)";
            System.out.println(u.getNome());
            preparedStatement = connection.prepareStatement(sql);

    preparedStatement.setString(1, u.getNome());
    preparedStatement.setString(2, u.getEmail());
    preparedStatement.setString(3, u.getSenha());
    preparedStatement.setDate(4, startDate);
    preparedStatement.setBoolean(5, true);
    preparedStatement.setInt(6, 2);
    preparedStatement.execute();

    // this.preparedStatement.executeQuery();


        }
        catch (SQLException e){
            e.printStackTrace();
        }

        return u;
    }
}