package br.csi.controller;

import br.csi.model.Usuario;
import br.csi.service.UsuarioService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("cad")

public class UsuarioController extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String nome = req.getParameter("nome");
        String email = req.getParameter("email");
        String senha = req.getParameter("senha");

        Usuario u = new Usuario();

        RequestDispatcher rd;

        if (nome.isEmpty()||email.isEmpty()||senha.isEmpty()){
            System.out.println("Erro");

            /*resp.sendRedirect("/app_bic/cad");*/

            req.setAttribute("erro","Todos os campos devem ser preenchidos!");
            rd = req.getRequestDispatcher("/WEB-INF/cadastro.jsp"); //DEVERIA VOLTAR A TELA INICIAL COM ERRO//

        }
        else {
            u.setNome(nome);
            u.setEmail(email);
            u.setSenha(senha);
            new UsuarioService().cadastro(u);

            rd = req.getRequestDispatcher("/WEB-INF/login.jsp");


        }
        rd.forward(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher rd;
        rd = req.getRequestDispatcher("/WEB-INF/cadastro.jsp");
        rd.forward(req, resp);
    }
}